#!/bin/bash
version="0.1.0"

source ./inc/get/colors.sh

# === MESSAGES
get_message_light()
{
  MSG=$1
  echo -e "$(get_color_light)$MSG$(get_color_default)\n"
}

get_message_info()
{
  MSG=$1
  echo -e "$(get_color_info)$MSG$(get_color_default)\n"
}

get_pwd ()
{
  MY_PWD=$(pwd)
  get_message_info ${MY_PWD}
}
# === END MESSAGES

# === BUILDING
what_will_happen ()
{
  get_message_info "SYMFONY PROJET CREATION v${version}";
  get_message_info "This script will create Symfony project, full web or microservice (ex. API)";
  get_message_info "It will install Symfony (no kidding)…\nBut also :";
  echo -e "  - API Platform";
  echo -e "  - Maker bundle";
  echo -e "  - Migration bundle";
  echo -e "\n";
  get_message_info "A 'git commit' will be performed for each step.";
}

create_symfony_project ()
{
  read -p "Choose a project name (avoid spaces) Ex. my-project-name : " project_name;
  echo -e "Creating Symfony project :";

  symfony check:requirements;

  read -p "Create a full web project (y/N) ? " choice;

  if [ "$choice" = "y" ]; then
    get_message_info "Creating a traditionnal web application";
    symfony new $project_name --full;
  else
    get_message_info "Creating a microservice application or API";
    symfony new $project_name;
  fi

  cd $project_name;

  git init;
  git add .;
  git commit -am "Initial commit : created Symfony project."
}

create_locale_env () {
  get_message_info "Creating .env file for dev";
  cp .env .env.local
  get_message_info "TODO : set DB settings in .env.local.php file."
}

install_api () {
  get_message_info "Installing API Platform";
  composer req api;
  git add .;
  git commit -am "Installed API Platform";
}

install_maker_bundle () {
  get_message_info "Installing symfony/maker-bundle";

  composer require symfony/maker-bundle --dev;

  git add .;
  git commit -am "Installed symfony/maker-bundle";

  get_message_info "You will be able to create entity executing this command :";
  get_message_light "    bin/console make:entity";
}

install_migration_bundle () {
  get_message_info "Installing migration bundle";

  composer require migrations;

  git add .;
  git commit -am "Installed migration bundle";

  get_message_info "You will be able to create migrations executing this command :";
  get_message_light "    bin/console make:migration";
}
# === END BUILDING
