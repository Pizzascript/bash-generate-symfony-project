#!/bin/bash

function get_color_black ()
{
  echo '\033[0;30m'
}
function get_color_red ()
{
  echo '\033[0;31m'
}
function get_color_green ()
{
  echo '\033[0;32m'
}
function get_color_orange ()
{
  echo '\033[0;33m'
}
function get_color_blue ()
{
  echo '\033[0;34m'
}
function get_color_purple ()
{
  echo '\033[0;35m'
}
function get_color_cyan ()
{
  echo '\033[0;36m'
}
function get_color_lightgrey ()
{
  echo '\033[0;37m'
}
function get_color_darkgrey ()
{
  echo '\033[1;30m'
}
function get_color_lightred ()
{
  echo '\033[1;31m'
}
function get_color_darkgreen ()
{
  echo '\033[1;32m'
}
function get_color_yellow ()
{
  echo '\033[1;33m'
}
function get_color_lightblue ()
{
  echo '\033[1;34m'
}
function get_color_lightpurple ()
{
  echo '\033[1;35m'
}
function get_color_lightcyan ()
{
  echo '\033[1;36m'
}
function get_color_white ()
{
  echo '\033[1;37m'
}
function get_color_nc ()
{
  echo '\033[0m' # No Color
}

# ===

function get_color_danger ()
{
  echo $(get_color_red)
}
function get_color_default ()
{
  echo $(get_color_nc)
}
function get_color_info ()
{
  echo $(get_color_cyan)
}
function get_color_light ()
{
  echo $(get_color_white)
}
function get_color_muted ()
{
  echo $(get_color_lightgrey)
}
function get_color_warning ()
{
  echo $(get_color_orange)
}

# ===

test_colors()
{
  printf "  - $(get_color_green)get_color_green$(get_color_default) -\n"
  printf "  - $(get_color_orange)get_color_orange$(get_color_default) -\n"
  printf "  - $(get_color_blue)get_color_blue$(get_color_default) -\n"
  printf "  - $(get_color_purple)get_color_purple$(get_color_default) -\n"
  printf "  - $(get_color_cyan)get_color_cyan$(get_color_default) -\n"
  printf "  - $(get_color_lightgrey)get_color_lightgrey$(get_color_default) -\n"
  printf "  - $(get_color_darkgrey)get_color_darkgrey$(get_color_default) -\n"
  printf "  - $(get_color_lightred)get_color_lightred$(get_color_default) -\n"
  printf "  - $(get_color_darkgreen)get_color_darkgreen$(get_color_default) -\n"
  printf "  - $(get_color_yellow)get_color_yellow$(get_color_default) -\n"
  printf "  - $(get_color_lightblue)get_color_liht_blue$(get_color_default) -\n"
  printf "  - $(get_color_lightpurple)get_color_lght_purple$(get_color_default) -\n"
  printf "  - $(get_color_lightcyan)get_color_ligh_cyan$(get_color_default) -\n"
  printf "  - $(get_color_lightcyan)get_color_lightcyan$(get_color_default) -\n"
  printf "  - $(get_color_white)get_color_white$(get_color_default) -\n"
  printf "  - $(get_color_nc)get_color_nc$(get_color_default) -\n"
  printf "  - ---\n"
  printf "  - $(get_color_danger)get_color_danger$(get_color_default) -\n"
  printf "  - $(get_color_default)get_color_default$(get_color_default) -\n"
  printf "  - $(get_color_info)get_color_info$(get_color_default) -\n"
  printf "  - $(get_color_light)get_color_light$(get_color_default) -\n"
  printf "  - $(get_color_muted)get_color_muted$(get_color_default) -\n"
  printf "  - $(get_color_warning)get_color_warning$(get_color_default) -\n"
}

test_color() # FAILS
{
  COLOR_NAME=$1
  # printf "  - $(get_color_light)$COLOR_NAME $(get_color_default)\n"
  COLOR_NAME_LOWER="%s" | awk '{ print toupper($COLOR_NAME) }'
  echo $COLOR_NAME_LOWER
  printf "  - $(get_color_info)$GETTER $COLOR_NAME - $COLOR_NAME_LOWER$(get_color_default)\n"
}
