#!/bin/bash

# 2020/03/29

# Get color vars
source ./inc/get/colors.sh

# Get functions
source ./inc/functions/functions.sh

# The script
what_will_happen
create_symfony_project
create_locale_env
install_api
install_maker_bundle
install_migration_bundle
